﻿using System;
using System.Configuration;
using log4net;

namespace CirclesIT.Console.ParallelTests
{
    internal class CmdArguments
    {
        private static readonly ILog log = LogManager.GetLogger("parallel-tests");

        private const string CmdArgumentHelp = "help";
        private const string CmdArgumentCleanup = "cleanup";
        // for below there are IdentifyXYZ corresponding methods
        private const string CmdArgumentIncludeCategories = "include";
        private const string CmdArgumentParallelTasks = "max-parallel-tasks";
        private const string CmdArgumentNunitConsolePath = "nunit-console-path";
        private const string CmdArgumentTestAssembly = "test-assembly";
        private const string CmdArgumentUniqueTestIdPattern = "unique-test-id-pattern";
        private const string CmdArgumentTestPriorityPattern = "test-priority-pattern";
        private const string CmdArgumentDefaultTestPriority = "default-test-priority";
        private const string CmdArgumentEnvironmentVarPassingUniqueTestId = "environment-var-name";
        private const string AllCategoriesRequested = "#All Categories within test assembly#";

        internal string NunitConsolePath { get; set; }
        internal int MaxDegreeOfParallelism { get; set; }
        internal string UniqueTestIdPattern { get; set; }
        internal string AssemblyContainingTestsPath { get; set; }
        internal string [] TestCategoriesRequested { get; set; }
        internal double DefaultTestPriority { get; set; }
        internal string EnvironmentVarPassingUniqueTestId { get; set; }
        internal string TestPriorityPattern { get; set; }

        internal bool IsHelpRequested { get; set; }
        internal bool IsCleanupRequested { get; set; }


        internal CmdArguments()
        {
            LoadConfigurationDefaults();
        }

        private void LoadConfigurationDefaults()
        {
            MaxDegreeOfParallelism = int.Parse(ConfigurationManager.AppSettings["maxDegreeOfParallelism"]);
            UniqueTestIdPattern = ConfigurationManager.AppSettings["uniqueTestIdPattern"];
            NunitConsolePath = ConfigurationManager.AppSettings["nunitConsolePath"];
            EnvironmentVarPassingUniqueTestId = ConfigurationManager.AppSettings["environmentVarPassingUniqueTestId"];
            TestPriorityPattern = ConfigurationManager.AppSettings["testPriorityPattern"];
        }

        internal void ProcessCommandLineArguments(string[] args)
        {
            try
            {

                IdentifyIsHelpRequested(args);
                IdentifyIsCleanupRequested(args);

                if (IsCleanupRequested || IsHelpRequested)
                    return;

                IdentifyUniqueTestIdPattern(args);
                IdentifyTestsAssemblyName(args);
                IdentifyRequestedTestCategories(args);
                IdentifyNunitConsolePath(args);
                IdentifyRequestedMaxParallelTasks(args);
                IdentifyRequestedDefaultTestPriority(args);
                IdentifyEnvironmentVarPassingUniqueTestId(args);
                IdentifyTestPriorityPattern(args);
            }
            catch
            {
                log.Debug("An error occured while processing command line parameters. Will exit.");
                Environment.Exit(0);
            }
        }

        private void IdentifyTestPriorityPattern(string[] args)
        {
            string testPriorityPattern = GetArgument(CmdArgumentTestPriorityPattern, args);
            if (testPriorityPattern != null)
                TestPriorityPattern = testPriorityPattern.Split('=')[1];
            // else will use default from app.config
        }

        private void IdentifyEnvironmentVarPassingUniqueTestId(string[] args)
        {
            string environmentVarPassingUniqueTestId = GetArgument(CmdArgumentEnvironmentVarPassingUniqueTestId, args);
            if (environmentVarPassingUniqueTestId != null)
                EnvironmentVarPassingUniqueTestId = environmentVarPassingUniqueTestId.Split('=')[1];
            // else will use default from app.config
        }

        private void IdentifyRequestedDefaultTestPriority(string[] args)
        {
            string requestedDefaultTestPriority = GetArgument(CmdArgumentDefaultTestPriority, args);
            if (requestedDefaultTestPriority != null)
                DefaultTestPriority = double.Parse(requestedDefaultTestPriority.Split('=')[1]);
            // else will the AVG value among all the test priorities identified after loading the test assembly
        }

        private void IdentifyIsCleanupRequested(string[] args)
        {
            if (GetArgument(CmdArgumentCleanup, args) != null)
            {
                IsCleanupRequested = true;
            }
        }

        private void IdentifyNunitConsolePath(string[] args)
        {
            string nunitConsolePathArgument = GetArgument(CmdArgumentNunitConsolePath, args);
            if (nunitConsolePathArgument != null)
                NunitConsolePath = nunitConsolePathArgument.Split('=')[1];
            // else will use default from app.config
        }

        private void IdentifyUniqueTestIdPattern(string[] args)
        {
            string uniqueTestIdPattern = GetArgument(CmdArgumentUniqueTestIdPattern, args);
            if (uniqueTestIdPattern != null)
                UniqueTestIdPattern = uniqueTestIdPattern.Split('=')[1];
            // else will use default from app.config
        }

        private void IdentifyRequestedMaxParallelTasks(string[] args)
        {
            string maxParallelTasksArgument = GetArgument(CmdArgumentParallelTasks, args);
            if (maxParallelTasksArgument != null)
                MaxDegreeOfParallelism = int.Parse(maxParallelTasksArgument.Split('=')[1]);
            // else will use default from app.config
        }

        private  void IdentifyIsHelpRequested(string[] args)
        {
            if (GetArgument(CmdArgumentHelp, args) != null)
            {
                IsHelpRequested = true;
            }
        }

        private void IdentifyTestsAssemblyName(string[] args)
        {
            string assemblyArg = GetArgument(CmdArgumentTestAssembly, args);

            if (string.IsNullOrEmpty(assemblyArg))
                throw new InvalidOperationException("test-assembly not passed, cannot continue");

            AssemblyContainingTestsPath = assemblyArg.Substring(assemblyArg.IndexOf("=", StringComparison.Ordinal) + 1);
        }

        private void IdentifyRequestedTestCategories(string[] args)
        {
            string requestedTestCategories = GetArgument(CmdArgumentIncludeCategories, args);
            if (requestedTestCategories != null)
            {
                TestCategoriesRequested = requestedTestCategories.Split('=')[1].Split(',');
            }
            else
            {
                TestCategoriesRequested = new[] { AllCategoriesRequested }; // some internal name, ensuring that there will not be a test category with this name
            }
        }

        private string GetArgument(string arg, string[] args)
        {
            string argument = null;
            int argIndex = Array.FindIndex(args, s => s.ToLower().Contains(arg.ToLower()));

            if (argIndex > -1)
                argument = args[argIndex];

            return argument;
        }

        internal bool IsRequestedSpecificTestCategory()
        {
            return TestCategoriesRequested.Length == 1 && TestCategoriesRequested[0].Equals(AllCategoriesRequested);
        }
    }
}
