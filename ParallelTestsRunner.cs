﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace CirclesIT.Console.ParallelTests
{
    internal class ParallelTestsRunner
    {
        private static readonly ILog log = LogManager.GetLogger("parallel-tests");
        private readonly CmdArguments _cmdArguments;
        private readonly ScheduledTestsContainer _scheduledTestsContainer;
        private readonly ChildProcessManager _childProcessManager;

        public ParallelTestsRunner()
        {
            _cmdArguments = new CmdArguments();
            _scheduledTestsContainer = new ScheduledTestsContainer(_cmdArguments);
            _childProcessManager = new ChildProcessManager(_cmdArguments);
        }

        internal void Run(string [] args)
        {
            _cmdArguments.ProcessCommandLineArguments(args);

            if (_cmdArguments.IsCleanupRequested)
            {
                CleanupAndExit();
            }

            if (_cmdArguments.IsHelpRequested)
            {
                PrintHelpAndExit();
            }

            _scheduledTestsContainer.LoadTestsForExecution();

            if (_scheduledTestsContainer.NotFoundTestsWarnings != null &&
                _scheduledTestsContainer.NotFoundTestsWarnings.Count > 0)
            {
                log.Debug("WARNINGS occured:");
                foreach (var warning in _scheduledTestsContainer.NotFoundTestsWarnings)
                {
                    log.WarnFormat("Warning while processing tests: {0}", warning);
                }
            }

            _childProcessManager.DeletePidFile();
            // actually this is not for registering child process Id, but rather the main app process id
            // we need it in order to start tracing the process tree from it (when cleanup is requested)
            _childProcessManager.RegisterChildProcessPid(Process.GetCurrentProcess().Id);

            StartExecutingTests();
            // after successful execution, delete the pid file
            _childProcessManager.DeletePidFile();
        }

        private void PrintHelpAndExit()
        {
            System.Console.WriteLine(
                "Usage:\n"+
                "\n"+
                "---\n" +
                "Simplest form is (using default values for other parameters):\n"+
                "parallel-tests --test-assembly=C:\\Path\\To\\Test\\Assembly\n"+
                "Where C:\\Path\\To\\Test\\Assembly is the file system path to the DLL that was compiled from a particular automation testing projects\n"+
                "---\n" +

                @"parallel-tests --test-assembly:C:\Path\To\Test\Assembly [--include=category1,category2,categoryN] [--max-parallel-tasks=7] [--unique-test-id-pattern=uniqueTestId] [--test-priority-pattern=priority:] [--default-test-priority=200] [--environment-var-name=MY_OWN_VAR_NAME]\n"+
                "\n"+
                "Command line arguments\n"+
                "--help - displays help usage\n"+
                "\n"+
                "--cleanup - reads the PID.txt file and trace the process tree to kill all child processes\n"+
                "\n"+
                "--include - comma separated list of test categories to execute (if empty will execute all test methods decorated by TestAttribute )\n"+
                "\n"+
                "--max-parallel-tasks - how many processes to spawn simultaneously\n"+
                "\n"+
                "--nunit-console-path - OS path to nunit-console.exe (default is pointing to packages folder inside this repo)\n"+
                "\n"+
                "--test-assembly -- OS path to assembly to load and examine tests\n"+
                "\n"+
                "--unique-test-id-pattern - specify how to identify each particular test, i.e what to search for in the CategoryAttributes, default is caseid:\n"+
                "\n"+
                "--test-priority-pattern - specify which CategoryAttribute to search for identifying test priorities, default is priority:\n"+
                "\n"+
                "--default-test-priority - what value to set for prioriy on tests that are to be executed but do not have priority set (if not set default value will be computed as Average of all other priorities identified)\n"+
                "\n"+
                "--environment-var-name - for each process spawn, there will be Environment variable set which will contain the data after --include arg passed to nunit-console.exe (default is TEST_EXECUTING_CATEGORY)\n");

            Environment.Exit(0);
        }

        private void StartExecutingTests()
        {
            var sb = new StringBuilder();
            sb.AppendLine("===================================================================================");
            sb.AppendLine("                            Running tests in parallel                              ");
            sb.AppendLine("===================================================================================");
            int totalTests = 0;
            foreach (var priority in _scheduledTestsContainer.TestIdsToRun.Keys)
            {
                totalTests += _scheduledTestsContainer.TestIdsToRun[priority].Count;
            }

            sb.AppendLine(string.Format("Total tests scheduled for execution: {0}", totalTests));
            sb.AppendLine(string.Format("Test assembly loaded: {0}", _cmdArguments.AssemblyContainingTestsPath));
            sb.AppendLine(string.Format("Max degree of parallelism: {0}", _cmdArguments.MaxDegreeOfParallelism));
            sb.AppendLine(string.Format("nunit-console.exe path: {0}", _cmdArguments.NunitConsolePath));
            sb.AppendLine(string.Format("Test categories requested: {0}", string.Join(",", _cmdArguments.TestCategoriesRequested)));
            sb.AppendLine(string.Format("Unique test Id pattern: {0}", _cmdArguments.UniqueTestIdPattern));
            sb.AppendLine(string.Format("Env variable name passed to child processes: {0}", _cmdArguments.EnvironmentVarPassingUniqueTestId));
            sb.AppendLine(string.Format("Default test priority: {0}", _cmdArguments.DefaultTestPriority));
            sb.AppendLine(string.Format("Priority pattern to use: {0}", _cmdArguments.TestPriorityPattern));

            sb.AppendLine("all uniqueTestIds to run by priorities:");
            foreach (var priority in _scheduledTestsContainer.TestIdsToRun.Keys)
            {
                sb.Append(string.Format("\n priority {0}:\n{1}", priority,
                    string.Join("\n", _scheduledTestsContainer.TestIdsToRun[priority])));
            }

            log.DebugFormat("Starting parallel-tasks with configuration: \n{0}", sb);

            var processesExitCodesCollection = new ConcurrentBag<int>();
            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = _cmdArguments.MaxDegreeOfParallelism };

            foreach (var priority in _scheduledTestsContainer.TestIdsToRun.Keys)
            {
                var watch = new Stopwatch();
                watch.Start();

                List<ScheduledTest> allTestsWithPriority = _scheduledTestsContainer.TestIdsToRun[priority];

                Parallel.ForEach(allTestsWithPriority, parallelOptions, scheduledTest => 
                {
                    log.Debug("Will start new OS process... List of all currently running processes is : " + _childProcessManager.RunningChildProcessPids);
                    processesExitCodesCollection.Add(_childProcessManager.RunTests(scheduledTest.UniqueTestId, scheduledTest.TestMethodName));
                });

                watch.Stop();
                log.DebugFormat("Finished processing of tests: {0} in {1} miliseconds", String.Join(",", _cmdArguments.TestCategoriesRequested), watch.ElapsedMilliseconds);
            }

            log.Debug("Exit codes from processes: " + string.Join(",", processesExitCodesCollection.ToArray()));

            _childProcessManager.DeletePidFile();

            if (processesExitCodesCollection.ToArray().Any(res => res != 0))
                Environment.Exit(1);

            Environment.Exit(0);
        }

        private void CleanupAndExit()
        {
            if (File.Exists(_childProcessManager.PidFile))
            {
                string[] pids = File.ReadAllLines(_childProcessManager.PidFile);
                foreach (var pid in pids)
                {
                    UInt32 lastRunPid = UInt32.Parse(pid);
                    _childProcessManager.KillAllProcessesSpawnedByLastRun(lastRunPid);
                }
            }
            else
            {
                log.Debug("There is no PID file found");
            }

            Environment.Exit(0);
        }
    }
}
