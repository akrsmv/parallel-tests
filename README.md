C#, Nunit tool for parallel tests execution
==============================================

parallel-tests.exe is a small console application providing parallel execution of the tests contained in particular assembly that is provided as a command line parameter. 

It uses reflection to identify what tests are required to be executed and then for each identified test spawns a separate OS process which executes nunit-console.exe with corresponding parameters. The number of OS processes that are simultaneously spawn, test categories to be executed, path of the assembly containing tests etc. are to be provided as command line arguments to the parallel-tests.exe (for some of them there are default values in App.Config). 

###In order to use the tool with your tests, you should make use of the @CategoryAttribute() c# Attribute. Please read further

###The simplest form of using the tool is

```
parallel-tests --test-assembly=C:\Path\To\Test\Assembly
```

Where `C:\Path\To\Test\Assembly` is the file system path to the DLL that was compiled from a particular automation testing projects.

By passing some additional command line parameters, other useful features of the tool may be used:
- Executing only a particular test category (i.e only a group of tests, contained in the test assembly, say only the player registration tests)
- Test cases prioritizing - some times it is needed some tests to be executed prior others. 
- Controlling max degree of parallelism, i.e how many tests to run at once

Below are given details on how to use those additional features.

##Advanced Usage

parallel-tests.exe may be used for any other assembly containing nunit test definitions, as long as each test is decorated with additional CategoryAttribute with value of the form uniqueTestId:xyz (uniqueTestId is a static string, and xyz is some unique sequence within the context of the test assembly)

Once it is ensured that each test is decorated with [CategoryAttribute("uniqueTestId:xyz")], one can execute the command line tool like this:

```
parallel-tests --include=someCategory --test-assembly=C:\Path\To\Test\Assembly
```

In the above example, assuming that they are 28 tests decorated with [CategoryAttribute("someCategory")], and each of them is additionally decorated with [CategoryAttribute("uniqueTestId:xyz")], the tool will load the test assembly, will examine all test methods, by categories (in this case will load only those decorated with BonusTool category attribute), and will register the above 28 methods for execution - by their category caseid:xyz. If there is no --include command line parameter provided, the tool will execute all the tests from the passed test assembly


After tests to be run were identified by their uniqueTestId:, for each of them the tool will spawn a process calling:
nunit-console.exe C:\Path\To\Test\Assembly --include:uniqueTestId:xyz

In this case default values will be loaded for max-degree-of-parallelism, the unique test category pattern

Below example shows how one can override the defaults 

```
parallel-tests --include=someCategory --test-assembly=C:\Path\To\Test\Assembly --max-parallel-tasks=7 --unique-test-id-pattern=uniqueTestId --test-priority-pattern=priority: --default-test-priority=200 --environment-var-name=MY_OWN_VAR_NAME
```

##Command line arguments
*--help* - displays help usage

*--cleanup* - reads the PID.txt file and trace the process tree to kill all child processes

*--include* - comma separated list of test categories to execute (if empty will execute all test methods decorated by TestAttribute )

*--max-parallel-tasks* - how many processes to spawn simultniously

*--nunit-console-path* - OS path to nunit-console.exe (default is pointing to packages folder inside this repo)

*--test-assembly* -- OS path to assembly to load and examine tests

*--unique-test-id-pattern* - specify how to identify each particular test, i.e what to search for in the CategoryAttributes, default is caseid:

*--test-priority-pattern* - specify which CategoryAttribute to search for identifying test priorities, default is priority:

*--default-test-priority* - what value to set for prioriy on tests that are to be executed but do not have priority set (if not set default value will be computed as Average of all other priorities identified)

*--environment-var-name* - for each process spawn, there will be Environment variable set which will contain the data after --include arg passed to nunit-console.exe (default is TEST_EXECUTING_CATEGORY)

*Note that the symboll = is used to properly identify the cmd arguments. Currently it cannot be used as part of the CategoryAttribute name.*


##Test priorities

If there are some dependencies between tests, for ex. first should execute Deposit and after that Withdrawal, one can achieve that by using the support for test priorities. A test priority with value 1 will be threated as highest priority

##Cancel and kill all processes
If a cancelation of tests is needed, one can execute

```
parallel-tests.exe --cleanup
```

This will trace all child processes that were spawn by the last tool execution and will kill them

##Team City Integration

In order for proper displaying for running tests in team city builds, for each process spawn, the tool will log into the console messages that are understandable by team city:
```
##teamcity[testSuiteStarted name='Name_Of_Test_method_executing (uniqueTestId:xyz)']
.....[specific test messages goes here].......
##teamcity[testSuiteFinished name='Name_Of_Test_method_executing (uniqueTestId:xyz)']
```

If using this tool, in order to report failed/successful tests, from within the test assembly containing the actual tests, one should print to Console.stdout messages of the form:

```
##teamcity[testStarted name='testname']
<here go all the test service messages with the same name>
##teamcity[testFinished name='testname' duration='test_duration_in_milliseconds'] OR: 
##teamcity[testFailed name='testname' message='failure message' details='message and stack trace']
```
more info: https://confluence.jetbrains.com/display/TCD4/Build+Script+Interaction+with+TeamCity#BuildScriptInteractionwithTeamCity-ReportingTests