﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using NUnit.Framework;

namespace CirclesIT.Console.ParallelTests
{
    internal class ScheduledTestsContainer
    {

        private static readonly ILog log = LogManager.GetLogger("parallel-tests");
        private CmdArguments _cmdArguments;
        private readonly List<ScheduledTest> _unorderedScheduledTests = new List<ScheduledTest>();
        internal Dictionary<double, List<ScheduledTest>> TestIdsToRun { get; set; }

        internal List<string> NotFoundTestsWarnings { get; set; }

        internal ScheduledTestsContainer(CmdArguments cmdArguments)
        {
            _cmdArguments = cmdArguments;
        }

        internal void LoadTestsForExecution()
        {
            Assembly assemblyContainingTests = Assembly.LoadFrom(_cmdArguments.AssemblyContainingTestsPath);

            foreach (Type type in assemblyContainingTests.GetTypes())
            {
                MethodInfo[] methods = type.GetMethods();

                foreach (var methodInfo in methods)
                {
                    if (MethodIsRequestedTestToRun(methodInfo))
                    {
                        RegisterTestForExecution(methodInfo);
                    }
                }
            }

            OrganizeScheduledTests();
        }

        private void OrganizeScheduledTests()
        {
            TestIdsToRun =  new Dictionary<double, List<ScheduledTest>>();

             //by now we have all tests with their priorities assigned and optionaly some tests without a priority
            List<double> allNotNullPriorities = (from test in _unorderedScheduledTests
                                                 where test.PriorityValue != null
                                                 select (double)test.PriorityValue).Distinct().ToList();
                                   

            if (allNotNullPriorities.Count > 0)
            {
                allNotNullPriorities.Sort();

                if (_cmdArguments.DefaultTestPriority == 0)
                    _cmdArguments.DefaultTestPriority = allNotNullPriorities.Average();

                // walk all Scheduled Tests and if one has no priority, assign it the default computed
                foreach (var scheduledTest in _unorderedScheduledTests)
                {
                    if (scheduledTest.PriorityValue == null)
                    {
                        scheduledTest.PriorityValue = _cmdArguments.DefaultTestPriority;
                    }
                }
                // now we have all the tests with assigned priorities - either from the allNotNullPriorities list or the defaultPriorityValue
                // walk thru the list of priorities

                bool defaultPriorityProcessed = false;
                foreach (var priority in allNotNullPriorities)
                {
                    if (priority > _cmdArguments.DefaultTestPriority && !defaultPriorityProcessed)
                    {
                        List<ScheduledTest> allTestsWithDefaultPriority =
                            (from tests in _unorderedScheduledTests
                             where tests.PriorityValue == _cmdArguments.DefaultTestPriority
                             select tests).ToList();
                        TestIdsToRun.Add(_cmdArguments.DefaultTestPriority, allTestsWithDefaultPriority);
                        defaultPriorityProcessed = true;
                    }
                    if (priority.Equals(_cmdArguments.DefaultTestPriority) && !defaultPriorityProcessed)
                    {
                        defaultPriorityProcessed = true; // then values will be added by the below line
                    }

                    List<ScheduledTest> allTestsWithThisPriority =
                        (from tests in _unorderedScheduledTests where tests.PriorityValue == priority select tests).ToList();

                    TestIdsToRun.Add(priority, allTestsWithThisPriority);
                }
                if (!defaultPriorityProcessed) // if the default priority is some biig value bigger than any other priority, we should append all default priority tests to the end
                {
                    List<ScheduledTest> allTestsWithDefaultPriority =
                            (from tests in _unorderedScheduledTests
                             where tests.PriorityValue == _cmdArguments.DefaultTestPriority
                             select tests).ToList();
                    TestIdsToRun.Add(_cmdArguments.DefaultTestPriority, allTestsWithDefaultPriority);
                }
            }
            else
            {
                TestIdsToRun.Add(1, _unorderedScheduledTests);
            }
        }

        private bool MethodIsRequestedTestToRun(MethodInfo method)
        {
            var methodAttributes = method.GetCustomAttributes();

            #region DEBUG LOG
            //log.DebugFormat("Analysing method {0}", method.Name);
            //log.DebugFormat("All attributes of {0}:", method.Name);
            //foreach (var attribute in methodAttributes)
            //{
            //    log.DebugFormat("{0}", attribute.ToString());
            //    if (IsCategoryAttribute(attribute))
            //    {
            //        log.DebugFormat("\tThis is CategoryAttribute with value: {0}", (attribute as CategoryAttribute).Name);
            //    }
            //}
            #endregion DEBUG

            if (methodAttributes.Any(IsTestAttribute))
            {
                #region debug
                var uqName = "";
                
                Attribute debugUniqueId =
                    methodAttributes.SingleOrDefault(
                        c =>
                            c is CategoryAttribute &&
                            (c as CategoryAttribute).Name.ToLower().Contains(_cmdArguments.UniqueTestIdPattern.ToLower()));
                if (debugUniqueId != null)
                {
                    uqName = (debugUniqueId as CategoryAttribute).Name;
                }
                // so its a test method
                log.DebugFormat("{0} is a nunit test method. Its Id is {1}. Checking if it is requested to be executed...", method.Name, uqName);
                #endregion debug
                if (_cmdArguments.IsRequestedSpecificTestCategory())
                {
                    // the case when no CmdArgumentIncludeCategories was passed
                    //log.DebugFormat("{0} is requested to be executed...", method.Name);
                    return true;
                }

                if (methodAttributes.Any(attr => (IsCategoryAttribute(attr) && _cmdArguments.TestCategoriesRequested.Any(
                    category => category.ToLower().Equals((attr as CategoryAttribute).Name.ToLower())))))
                {
                    //log.DebugFormat("{0} is requested to be executed...", method.Name);
                    return true;
                }
            }
            //log.DebugFormat("{0} is not a nunit test method. Will skip it.", method.Name);
            return false;
        }

        private void RegisterTestForExecution(MethodInfo method)
        {
            var methodAttributes = method.GetCustomAttributes();

            try
            {
                Attribute uniqueIdCategory = methodAttributes.Where(attr =>
                    IsCategoryAttribute(attr) &&
                    (attr as CategoryAttribute).Name.Contains(_cmdArguments.UniqueTestIdPattern)).Single();

                if (uniqueIdCategory != null)
                {
                    //so it has a unique id and can be scheduled
                    string uniqueTestId = (uniqueIdCategory as CategoryAttribute).Name;

                    Attribute testPriorityCategory = null;

                    if (methodAttributes.Any(attr =>
                        IsCategoryAttribute(attr) &&
                        (attr as CategoryAttribute).Name.Contains(_cmdArguments.TestPriorityPattern)))
                    {
                        testPriorityCategory = methodAttributes.Where(attr =>
                            IsCategoryAttribute(attr) &&
                            (attr as CategoryAttribute).Name.Contains(_cmdArguments.TestPriorityPattern)).Single();
                    }

                    // further analyzing of test priority happens inside the ScheduledTest object
                    double? priority = null;
                    if (testPriorityCategory != null)
                    {
                        var stringValue = (testPriorityCategory as CategoryAttribute).
                            Name.Replace(_cmdArguments.TestPriorityPattern, "").Replace("=", "");
                        priority = int.Parse(stringValue);
                    }
                    _unorderedScheduledTests.Add(new ScheduledTest(uniqueTestId, method.Name, priority));
                }
            }
            catch
            {
                NotFoundTestsWarnings.Add(
                    string.Format("For Test method {0}, was unable to find unique identifier category", method.Name));
            }
        }

        private bool IsTestAttribute(Attribute attr)
        {
            return attr.GetType() == typeof(TestAttribute);
        }

        private bool IsCategoryAttribute(Attribute attr)
        {
            return attr.GetType() == typeof(CategoryAttribute);
        }
    }
}
