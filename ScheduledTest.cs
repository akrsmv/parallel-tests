﻿namespace CirclesIT.Console.ParallelTests
{
    internal class ScheduledTest
    {
        internal string UniqueTestId { get; set; }
        internal string TestMethodName { get; set; }
        internal double? PriorityValue { get; set; }

        public ScheduledTest(string uniqueId, string testMethodName, double? priority)
        {
            UniqueTestId = uniqueId;
            TestMethodName = testMethodName;
            PriorityValue = priority;
        }

        public override string ToString()
        {
            return string.Format("{{ID: {0}, Method: {1}}}", UniqueTestId, TestMethodName);
        }
    }
}
