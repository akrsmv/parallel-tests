﻿using System.IO;
using log4net.Config;

namespace CirclesIT.Console.ParallelTests
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure(new FileInfo("log4net.config"));
            var parallelTests = new ParallelTestsRunner();
            parallelTests.Run(args);
        }
    }
}
