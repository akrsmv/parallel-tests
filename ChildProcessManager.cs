﻿using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Text;
using log4net;
using log4net.Util;
using System.Collections.Concurrent;


namespace CirclesIT.Console.ParallelTests
{
    internal class ChildProcessManager
    {
        private static readonly ILog log = LogManager.GetLogger("parallel-tests");
        internal string PidFile = "PID.txt";
        private readonly ReaderWriterLock rwl = new ReaderWriterLock();
        private CmdArguments _cmdArguments;

        private ConcurrentBag<int> _childPIDs = new ConcurrentBag<int>();

        public string RunningChildProcessPids 
        {
            get { return string.Join(", ", _childPIDs); }
        }

        public ChildProcessManager(CmdArguments cmdArguments)
        {
            _cmdArguments = cmdArguments;
        }

        internal int RunTests(string uniqueId, string testMethodName)
        {
            StringBuilder output = new StringBuilder(string.Format("Prepare to spawn new process for running test with Id {0}", uniqueId));

            output.Append(string.Format("##teamcity[testSuiteStarted name='{0}({1})']", testMethodName, uniqueId));
            output.Append(string.Format("Tests identified by Category " + uniqueId + " standard output follows "));

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var process = new Process();

            process.StartInfo = new ProcessStartInfo(_cmdArguments.NunitConsolePath,
                    string.Format(@"{0} --include:{1}",
                        _cmdArguments.AssemblyContainingTestsPath, uniqueId));
            process.StartInfo.EnvironmentVariables.Add(_cmdArguments.EnvironmentVarPassingUniqueTestId, uniqueId);

            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;

            process.Start();
            int pid = process.Id;
            RegisterChildProcessPid(pid);
            output.Append(process.StandardOutput.ReadToEnd());

            stopwatch.Stop();

            process.WaitForExit();

            DeregisterChildProcessPid(pid);

            output.Append(string.Format(("##teamcity[testSuiteFinished name='[{0}({1})']"), testMethodName, uniqueId));
            output.Append("Tests identified by Category " + uniqueId + " finished in " + stopwatch.ElapsedMilliseconds + " miliseconds");

            // EDIT: more DEBUG INFO requested by QA team: for some reason some of the exit codes are -100, which means "unexpected error" and is related to nunit-console.exe internals
            // we bypass those exit codes, replacing them with 0 as this is not of interest for us (all tests seem to operate properly, despite the -100 error code)
            if (process.ExitCode < 0)
            {
                output.Append(string.Format("[QA-WARNING]: Unexpected error (code: {0}) occurred while running Test: {1} which was with unique ID: {2}. Process ID: {3}", process.ExitCode, testMethodName, uniqueId, pid));
                // also for the specific -100 error, we try to bypass TC failures by just returning 0 instead of -100
                // (I think TC would still fail as here we are only reporting error codes to console, however TC reads them from elsewhere)
                if (process.ExitCode == -100)
                {
                    log.Debug(output);
                    return 0;
                }
            }

            log.Debug(output);

            return process.ExitCode;
        }

        internal void DeletePidFile()
        {
            if (File.Exists(PidFile))
                File.Delete(PidFile);
        }

        internal void RegisterChildProcessPid(int pid)
        {
            rwl.AcquireWriterLock();
            _childPIDs.Add(pid);
            File.WriteAllText(PidFile, string.Join("\n", _childPIDs));
            rwl.ReleaseWriterLock();
        }

        internal void DeregisterChildProcessPid(int pid)
        {
            rwl.AcquireWriterLock();
            if (_childPIDs.TryTake(out pid))
            {
                File.WriteAllText(PidFile, string.Join("\n", _childPIDs));
            }
            else
            {
                log.Error("Error trying to deregister PID file: " + pid);
            }
            
            rwl.ReleaseWriterLock();
        }

        internal void KillAllProcessesSpawnedByLastRun(UInt32 parentProcessId)
        {
            log.Debug("Finding processes spawned by process with Id [" + parentProcessId + "]");

            var searcher = new ManagementObjectSearcher(
                "SELECT * " +
                "FROM Win32_Process " +
                "WHERE ParentProcessId=" + parentProcessId);
            ManagementObjectCollection collection = searcher.Get();
            if (collection.Count > 0)
            {
                log.Debug("Killing [" + collection.Count + "] processes spawned by process with Id [" + parentProcessId + "]");
                foreach (var item in collection)
                {
                    UInt32 childProcessId = (UInt32)item["ProcessId"];
                    if ((int)childProcessId != Process.GetCurrentProcess().Id) // actually it should not happen that the current process PID is in the list, but in any case
                    {
                        KillAllProcessesSpawnedByLastRun(childProcessId);

                        try
                        {
                            Process childProcess = Process.GetProcessById((int)childProcessId);
                            log.Debug("Killing child process [" + childProcess.ProcessName + "] with Id [" + childProcessId + "]");

                            childProcess.Kill();
                        }
                        catch (Exception exception)
                        {
                            log.Debug(exception.Message);
                        }
                    }
                }
            }
        }
    }
}
